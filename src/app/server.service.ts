import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServerService {
  constructor(private http: Http) {}
  storeServers(servers: any[]) {
    const headers = new Headers({'Content-Type': 'application/json'});
    // return this.http.post('https://udemy-ng-http.firebaseio.com/data.json',
    //   servers,
    //   {headers: headers});
    return this.http.put('https://udemy-ng-http.firebaseio.com/data.json',
      servers,
      {headers: headers});
  }
  getServers() {
    return this.http.get('https://udemy-ng-http.firebaseio.com/data')
      .map(
        (response: Response) => {
          const data = response.json();
          for (const server of data) {
            server.name = 'FETCHED_' + server.name;
          }
          return data;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong');
        }
      );
  }
  getAppName() {
    return this.http.get('https://udemy-ng-http.firebaseio.com/appName.json')
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }
  getListUser(){
     return this.http.get('http://localhost:3000/api/Users').map(
      (response: Response) => {
        return response.json();
      }
    );
  }
  getETH(){
    return this.http.get('https://www.coinbase.com/api/v2/prices/ETH-VND/historic?period=year').map(
      (response: Response) => {
        return response.json();
      }
    );
  }
  getHistoryProfit() {
    return this.http.get('http://localhost:1337/get/UserProfit').map(
      (response: Response) => {
        return response.json();
      }
    );
    // let data = [{"date":"2018-07-07T11:46:35.404Z","changedVolume":2,"flag":3,"price":22865,"name":"Messi"},
    // {"date":"2018-07-07T01:50:20.119Z","changedVolume":10,"flag":3,"price":22865,"name":"Messi"},
    // {"date":"2018-07-14T03:46:35.404Z","changedVolume":2,"flag":3,"price":22865,"name":"Lamer"},
    // {"date":"2018-07-17T21:50:20.119Z","changedVolume":10,"flag":3,"price":22865,"name":"Lamer"},
    // {"date":"2018-07-13T15:46:35.404Z","changedVolume":2,"flag":3,"price":22865,"name":"Naruto"},
    // {"date":"2018-06-12T19:50:20.119Z","changedVolume":12,"flag":3,"price":22865,"name":"Sasuke"},
    // {"date":"2018-07-27T21:46:35.404Z","changedVolume":30,"flag":2,"price":22865,"name":"Messi"},
    // {"date":"2018-07-07T20:50:20.119Z","changedVolume":5,"flag":2,"price":22865,"name":"Messi"}];
    // return data; 
  }
  getHistoryProfitMock() {
    let data = [{"date":"2018-07-07T11:46:35.404Z","changedVolume":2,"flag":3,"price":22865,"name":"Messi"},
    {"date":"2018-07-07T01:50:20.119Z","changedVolume":10,"flag":3,"price":22865,"name":"Messi"},
    {"date":"2018-07-14T03:46:35.404Z","changedVolume":2,"flag":3,"price":22865,"name":"Lamer"},
    {"date":"2018-07-17T21:50:20.119Z","changedVolume":10,"flag":3,"price":22865,"name":"Lamer"},
    {"date":"2018-07-13T15:46:35.404Z","changedVolume":2,"flag":3,"price":22865,"name":"Naruto"},
    {"date":"2018-06-12T19:50:20.119Z","changedVolume":12,"flag":3,"price":22865,"name":"Sasuke"},
    {"date":"2018-07-27T21:46:35.404Z","changedVolume":30,"flag":2,"price":22865,"name":"Messi"},
    {"date":"2018-07-07T20:50:20.119Z","changedVolume":5,"flag":2,"price":22865,"name":"Messi"}];
    return data; 

  }
  private extractData(res: Response) {
    let body = res.json();
      return body;
  }
}
