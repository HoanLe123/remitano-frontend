import angular from 'angular';

angular.module('remitano.Constant', [])
	.constant('URLSERVICE', {
		getData: '/rest/remitano/data',
	})
export class ConstantModule {

	}