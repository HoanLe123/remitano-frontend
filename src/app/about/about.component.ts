import { Component } from '@angular/core';
import { ServerService } from '.././server.service';
import { Chart } from 'highcharts';
import { ChartModule } from 'angular2-highcharts';
import * as _ from 'lodash';
@Component({
    selector: 'about',
    styleUrls: ['./about.component.css'],
    templateUrl: './about.component.html'
})
export class AboutComponent {
    dataEth;
    chart: Object;
    options: Object;
    columns: Object;
    rows: Object;
    show; boolean = true;
    filterTable: Object;
    resultHistory;
    constructor(private serverService: ServerService) {
        let year = 2018;
        let data;
        this.show = true;
        // var lastDate = data[data.length - 1][0],  // Get year of last data point
        // days = 24 * 36e5; 
        this.options = {
            width: 1000,
            title: { text: 'simple chart' },

            series: [{
                name: 'AAPL Stock Price',
                data: data,
                id: 'dataseries',
                dataGrouping: {
                    units: [
                        [
                            'week', // unit name
                            [1] // allowed multiples
                        ], [
                            'month',
                            [1, 2, 3, 4, 6]
                        ],
                        [
                            'hour',
                            [1, 2, 3, 4, 6, 8, 12]
                        ]
                    ]
                }
            }],
            events: {
                load: function (event) {
                    event.target.reflow();
                }
            }
        };

        // console.log(Highcharts);

        let getdata = this.serverService.getETH().subscribe(data => {
            this.dataEth = data.data.prices;
            this.dataEth.forEach(function (part, index, data) {
                let value = data[index];
                data[index] = [Date.parse(value.time), value.price * 1];
            });
            let mockdata =

                [

                    [1147651200000, 67.79],
                    [1147737600000, 64.98],
                    [1147824000000, 65.26],
                    [1147910400000, 63.18],
                    [1147996800000, 64.51],
                    [1148256000000, 63.38],
                    [1148342400000, 63.15],
                    [1148428800000, 63.34],
                    [1148515200000, 64.33],
                    [1148601600000, 63.55],
                    [1148947200000, 61.22],
                    [1149033600000, 59.77]
                ]
            _.reverse(this.dataEth);
            this.options.series[0].data = mockdata;
            this.chart.series[0].setData(this.dataEth, true);

        });
        data = this.dataEth;
        //////////////////////////////test
        this.serverService.getHistoryProfit().subscribe(dataHistory1 => {
            let countData = 0;
            this.resultHistory = dataHistory1;
            dataHistory1.forEach(user => {
                if (user.history.length) {
                    user.history.forEach(element => {
                        let obj = {};
                        let sellBuy = element.flag == 3 ? 'sell' : 'buy';
                        let amount = element.changedVolume;
                        let color = element.flag == 3 ? 'red' : 'green';
                        obj = {
                            x: element.date,
                            title: user.username,
                            text: `${user.username} ${sellBuy} ${amount}`
                        }
                        let series = {
                            type: 'flags',
                            data: [obj],
                            shape: 'squarepin',
                            fillColor: color,
                            width: 10 + amount * 1.2,
                            onSeries: 'dataseries',
                            name: user.username
                        }
                        countData++;

                        this.options.series.push(series);

                    });
                }
            });
        });
        console.log(this.options.series);
        //////////////////////////////
        setTimeout(() => {

            // let dataHistory = this.serverService.getHistoryProfitMock();
            // let obj = {};
            // dataHistory.forEach(element => {
            //     let sellBuy = element.flag == 3 ? 'sell' : 'buy';
            //     let amount = element.changedVolume;
            //     let color = element.flag == 3 ? 'red' : 'green';
            //     obj = {
            //         x: new Date(element.date).getTime(),
            //         title: element.name,
            //         text: `${sellBuy} ${amount}`
            //     }
            //     let series = {
            //         type: 'flags',
            //         data: [obj],
            //         shape: 'squarepin',
            //         fillColor: color,
            //         width: 10 + amount * 1.2,
            //         onSeries: 'dataseries',
            //         name: element.name
            //     }
            //     this.options.series.push(series);
            // });


            this.filterTable = JSON.parse(JSON.stringify(this.options.series));


            /////////// this for table///
            this.columns = [
                { prop: 'name' },
                { prop: 'changedVolume' },
                { prop: 'flag' },
                { prop: 'date' }
            ];
            this.rows = this.resultHistory;;
            this.chart.series[0].setData(this.dataEth, true);
            console.log('aaaaaa');
            this.show = !this.show;
            setTimeout(() => {
                this.show = !this.show;
            }, 300);
            /////////////////////////////
        }, 1000);
    }

    updateFilter(event) {
        const nameFilter = event.target.value.toLowerCase();
        this.show = !this.show;
        // filter our data
        // console.log('this.filterTable', this.filterTable);
        // console.log(this.options.series);
        // console.log('Messi', nameFilter);
        // console.log(this.chart.series);
        let filterResult = _.map(this.filterTable, function (o) {

            if (o.name.toLowerCase() == nameFilter) {

                return o;
            }
        });
        filterResult = _.without(filterResult, undefined);
        let dataOrigin = this.options.series[0];
        filterResult.splice(0, 0, dataOrigin);
        // console.log('result filter', filterResult);
        console.log('this.options.series', this.options.series);

        // filterResult
        this.options.series = filterResult;

        // this.chart.series.splice(3,1);
        // this.options.series.splice(2, 1);
        this.chart.series[0].setData(this.dataEth, true);
        // update the rows
        // this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        // this.table.offset = 0;
        setTimeout(() => {
            this.show = !this.show;
        }, 300);

    }
    saveInstance(chartInstance) {
        this.chart = chartInstance;
        console.log(this.chart);
    }
}



